package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"github.com/trungtvq/latex/bussiness"
	"github.com/trungtvq/latex/external"
	"github.com/trungtvq/latex/monitor"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"

	"github.com/spf13/cobra"
)

var uploadCmd = &cobra.Command{
	Use:   "upload",
	Short: "upload service",
	Run: func(cmd *cobra.Command, args []string) {
		http.HandleFunc("/upload", uploadFile)
		fmt.Println("Service started at port: ", viper.GetString("server.port"))
		monitor.Logger.Bg().Info("Service is running at", zap.Any("port", viper.GetString("server.port")))
		http.ListenAndServe(":"+viper.GetString("server.port"), nil)
	},
}

//uploadFile .
func uploadFile(w http.ResponseWriter, r *http.Request) {
	monitor.Logger.Bg().Info("upload", zap.Any("header", r.Header.Get("Authorization")))
	// ParseToSimpleQuestion our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, _, err := r.FormFile("fileToUpload")
	if err != nil {
		monitor.Logger.Bg().Error("Error Retrieving the File", zap.Any("error:", err))
		w.WriteHeader(500)
	}
	defer file.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		monitor.Logger.Bg().Error("Error when read the File", zap.Any("error:", err))
	}

	//TODO: parse follow form solution
	//services.DetachList(string(fileBytes), `\Opensolutionfile{ans}`, `\Closesolutionfile`)

	bodyList, err := bussiness.FileToListQuestionBody(fileBytes)
	if err != nil {
		w.WriteHeader(500)
	}
	for _, body := range bodyList {
		external.InsertToDatabase(body, r.Header.Get("Authorization"))
	}

	j, _ := json.Marshal(bodyList)
	w.Write(j)

}
